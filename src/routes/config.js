import axios from "axios";
import {
  goto
} from "@sapper/app";

export const url = 'https://dev.telehost.ng/api';
export const header = (token) => {
  return {
    "Content-Type": "application/json",
    "Authorization": `Bearer ${token}`
  }
};
export const buy = (key) => {
  return {
    "Content-Type": "application/json",
    "Authorization": `${key}`
  }
};


export async function CHECK_AUTH(token) {
  axios
    .get(`${url}/protected`, {
      headers: header(token)
    })
    .then(res => {
      if (res.data.status === "Token is Expired") {
        localStorage.removeItem("currentUser");
        goto("/login");
        window.location.reload();
      } else {
        // console.log(res.data.message)
      }
    });
}

export async function GET_WALLET(token) {
  try {
    let wallet = await fetch(`${url}/wallet`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    wallet = await wallet.json();
    // console.log(wallet)

    return wallet;


    // if (wallet.msg !== 'success') throw wallet;

    return true;
  } catch (err) {
    // console.log(err);
  }
}


export async function GET_TRANSACTION(token) {
  try {
    let trans = await fetch(`${url}/my-transactions`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    trans = await trans.json();

    return trans;


    if (trans.msg !== 'success') throw trans;

    return true;
  } catch (err) {
    // console.log(err);
  }
}
export async function GET_RESPONSE(token) {
  try {
    let res = await fetch(`${url}/get-response`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    res = await res.json();

    return res;


    if (res.msg !== 'success') throw res;

    return true;
  } catch (err) {
    // console.log(err);
  }
}

export async function GET_SMS(token) {
  try {
    let sms = await fetch(`${url}/get-message`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    sms = await sms.json();

    return sms;


    if (sms.msg !== 'success') throw sms;

    return true;
  } catch (err) {
    // console.log(err);
  }
}
export async function GET_USSD(token) {
  try {
    let ussd = await fetch(`${url}/get-ussd`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    ussd = await ussd.json();

    return ussd;


    if (sms.msg !== 'success') throw sms;

    return true;
  } catch (err) {
    // console.log(err);
  }
}
export async function GET_USSD_PAGE(token,page) {
  try {
    let ussd = await fetch(`${url}/get-ussd?page=${page}`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    ussd = await ussd.json();

    return ussd;


    if (sms.msg !== 'success') throw sms;

    return true;
  } catch (err) {
    // console.log(err);
  }
}
export async function GET_DEVICES(token) {
  try {
    let devices = await fetch(`${url}/get-devices`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });
    devices = await devices.json();

    return devices.data;


    if (sms.msg !== 'success') throw sms;

    return true;
  } catch (err) {
    // console.log(err);
  }
}


export async function BUY_AIRTIME(data, token) {
  try {
    let user = await fetch(`${url}/buy-airtime`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data
    });
    user = await user.json();

    return user;


    if (user.msg === 'failed, wrong parameters') throw user;

    return true;
  } catch (err) {
    //console.log(err);

    return false;
  }
}
export async function BUY_SIM(data, token) {
  try {
    let user = await fetch(`${url}/buy-sim`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data
    });
    user = await user.json();

    //console(user);


    if (user.msg === 'failed, wrong parameters') throw user;

    return true;
  } catch (err) {
    //console.log(err);

    return false;
  }
}