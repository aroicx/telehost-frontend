import axios from "axios";
import {
    goto
} from "@sapper/app";
export const url = 'https://dev.telehost.ng/api';

export const header = (token) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
    }
};
export const buy = (key) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `${key}`
    }
};


export async function CHECK_AUTH(token) {
    axios
        .get(`${url}/protected`, {
            headers: header(token)
        })
        .then(res => {
            if (res.data.status === "Token is Expired") {
                localStorage.removeItem("currentUser");
                goto("/login");
                window.location.reload();
            } else {

            }
        });
}

export async function GET_USERS(token) {
    try {
        let users = await fetch(`${url}/admin/users`, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            method: 'GET'
        });
        users = await users.json();
        return users;
    } catch (err) {
        return err;
    }
}