import axios from "axios";
import {
    goto
} from "@sapper/app";
// export const url = 'http://telehost.test/api';
export const url = 'https://dev.telehost.ng/api';
export const header = (token) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
    }
};

export async function CHECK_AUTH(token) {
    axios
        .get(`${url}/protected`, {
            headers: header(token)
        })
        .then(res => {
            if (res.data.status === "Token is Expired") {
                localStorage.removeItem("currentUser");
                goto("/login");
                window.location.reload();
            }
        });
}





export async function IS_ADMIN(token) {
    try {
        let user = await fetch(`${url}/auth/check`, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            method: 'GET'
        });
        user = await user.json();
        return user;
    } catch (err) {
        return err;
        // console.log(err);
    }
}