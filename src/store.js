import { writable } from 'svelte/store'

const user = writable([]);

const userStore = {
    subscribe: user.subscribe,
    setUser: (currentUser) => {
        user.set(currentUser);
    },
    addCustomer: (customer) => {
        const newCustomer = {
            ...customer
        };
        customers.update(items => {
            return [newCustomer, ...items];
        });
    },
    updateUser: (id, user_data) => {
        user.update(items => {
            const userIndex = items.findIndex(i => i.id ===id);
            const updateduser = {...items[userIndex], ...user_data };
            const updatedusers = [...items];
            updatedusers[userIndex] = updateduser;
            return updatedusers;
        });
    },
    removeUser: (id) => {
        user.update(items => {
            console.log(items);
            items = [];
          
           
        });
    },
    
};

export default userStore;